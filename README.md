# Cars

Just a quick example app with basic car management operations. using React.js, Redux with an REST-API. 

## Instructions

Download or clone.
```
git clone https://404ricardo@bitbucket.org/404ricardo/cars.git
```
In the project directory, you can run:
```
npm install
npm start
```
Runs the app in the development mode.<br>
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

## Goals
- [ ] Add cars;
- [ ] Add sellers;
- [ ] Update sales;
- [ ] Reports.


Your suggestions are welcome :)
